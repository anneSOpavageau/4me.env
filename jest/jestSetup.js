import R from 'ramda';

import {
  printReceived,
  printExpected,
  matcherHint,
} from 'jest-matcher-utils';

import jestMatchers from 'jest-matchers';

// Based on https://github.com/facebook/jest/blob/master/packages/jest-matchers/src/matchers.js

const matchers = {
  toMatchSuggestions: () => ({
    compare: (unsortedActual, unsortedExpected = []) => {
      const actual = R.sortBy(
        R.join(','),
        R.map(R.sortBy(R.identity), unsortedActual)
      );
      const expected = R.sortBy(
        R.join(','),
        R.map(R.sortBy(R.identity), unsortedExpected)
      );

      expect(actual).toEqual(expected);

      return {
        pass: true,
      };
    },
  }),
  toMatchSortedSuggestions: () => ({
    compare: (unsortedActual, unsortedExpected = []) => {
      const actual = R.map(R.sortBy(R.identity), unsortedActual);
      const expected = R.map(R.sortBy(R.identity), unsortedExpected);

      expect(actual).toEqual(expected);

      return {
        pass: true,
      };
    },
  }),
};

jasmine.addMatchers(matchers);
