import getEnv, { ENVIRONMENTS } from '../../src';
import R from 'ramda';

describe('frequency data', () => {
  Object.keys(ENVIRONMENTS).forEach(env => {
    describe(`${env}`, () => {
      const {
        getFrequencies,
      } = getEnv(env).frequencies;

      describe('getFrequencies', () => {
        const frequencies = getFrequencies();

        test('should return a valid shaped object', () => {
          expect(Array.isArray(frequencies)).toBe(true);
          frequencies.forEach(freq => {
            expect(typeof freq).toBe('object');
            expect(freq.name).toBeDefined();
            expect(typeof freq.name).toBe('string');
          });
        });


        const {
          getElementarySectors,
        } = getEnv(env).sectors;

        const elementarySectors = getElementarySectors();

        frequencies.forEach(freq => {
          describe(`${freq.name}`, () => {
            test('should have a default sector', () => {
              expect(freq.defaultSector).toBeDefined();
              // We accept null as valid data here
              if(freq.defaultSector !== null) {
                expect(typeof freq.defaultSector).toBe('string');
              }
            });

            test('should have a defined default sector', () => {
              expect([null, ...elementarySectors]).toContain(freq.defaultSector);
            });
          });
        });

      });

    });
  });
});
