import React from 'react';
import { shallow } from 'enzyme';

import getEnv, { ENVIRONMENTS } from '../../../src';
import R from 'ramda';

const CwpButton = (props) => <div>Button</div>;
const RoomStatus = (props) => <div>RoomStatus</div>;

describe('cluster data', () => {
  Object.keys(ENVIRONMENTS).forEach(env => {
    describe(`${env}`, () => {
      const {
        getControlRoomLayout,
      } = getEnv(env).components;

      const ControlRoomLayout = getControlRoomLayout();

      describe('<ControlRoomLayout/>', () => {
        test('should throw if required props are not set', () => {
          const render = () => shallow(
            <ControlRoomLayout />
          );
          expect(render).toThrow();
        });

        test('should render correctly given a cwpButtonComponent prop', () => {
          const render = () => shallow(
            <ControlRoomLayout
              cwpButton={<CwpButton />}
            />
          );

          expect(render).not.toThrow();
        });

        test('should render at least one cwpButtonComponent', () => {
          const wrapper = shallow(
            <ControlRoomLayout
              cwpButton={<CwpButton />}
            />
          );

          expect(wrapper.find(CwpButton).length).toBeGreaterThan(1);
        });

        test('should render exactly one roomStatusComponent', () => {
          const wrapper = shallow(
            <ControlRoomLayout
              cwpButton={<CwpButton />}
              roomStatus={<RoomStatus />}
            />
          );

          expect(wrapper.find(RoomStatus).length).toBe(1);
        });

        test('should inject clientId into cwpButtonComponent', () => {
          const wrapper = shallow(
            <ControlRoomLayout
              cwpButton={<CwpButton />}
            />
          );

          wrapper.find(CwpButton).forEach(buttonWrapper => {
            expect(buttonWrapper.prop('clientId')).toBeDefined();
          });
        });
      });
    });
  });
});
