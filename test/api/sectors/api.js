import getSectorsAPI from '../../../src/sectors';

import { clusters, sectorGroups } from './mock';

const expectedApi = {
  getElementarySectors: 'function',
  getClusters: 'function',
  getSectorGroups: 'function',
  getSectorGroupByName: 'function',
  getSectorGroupByElementarySectors: 'function',
};

describe('getSectorsAPI', () => {
  test('should reject invalid data', () => {
    expect(() => getSectorsAPI()).toThrow();
    expect(() => getSectorsAPI('string')).toThrow();
    expect(() => getSectorsAPI({clusters})).toThrow();
    expect(() => getSectorsAPI({sectorGroups})).toThrow();
  });

  const API = getSectorsAPI({clusters, sectorGroups});

  test('should return an object', () => {
    expect(typeof API).toBe('object');
  });

  Object.keys(expectedApi).forEach(apiMethod => {
    describe(`${apiMethod}`, () => {
      test('should exist', () => {
        expect(API[apiMethod]).toBeDefined();
      });

      test(`should be a ${expectedApi[apiMethod]}`, () => {
        expect(typeof API[apiMethod]).toBe(expectedApi[apiMethod]);
      });
    });
  });
});
