export const clusters = [
  [['UR', 'XR', 'KR', 'HYR'], ['UN', 'UB']],
  [['E', 'SE']],
];

export const sectorGroups = [
  {
    name: 'UR',
    elementarySectors: ['UR'],
    canAccept: ['XR'],
    canGive: [],
  }, {
    name: 'XR',
    elementarySectors: ['XR'],
    canAccept: ['UR'],
    canGive: [],
  }, {
    name: 'KR',
    elementarySectors: ['KR'],
    canAccept: ['UXR', 'HYR'],
    canGive: [],
  }, {
    name: 'HYR',
    elementarySectors: ['HYR'],
    canAccept: ['KR', 'UXKR'],
    canGive: [],
  }, {
    name: 'UN',
    elementarySectors: ['UN'],
    canAccept: [],
    canGive: [],
  }, {
    name: 'UB',
    elementarySectors: ['UB'],
    canAccept:[],
    canGive: [],
  }, {
    name: 'E',
    elementarySectors: ['E'],
    canAccept:[],
    canGive: [],
  }, {
    name: 'SE',
    elementarySectors: ['SE'],
    canAccept:[],
    canGive: [],
  }, {
    name: 'UXNR',
    elementarySectors: ['UR', 'XR', 'UN', 'UB'],
    canAccept: [],
    canGive: ['UXR', 'UBN'],
  }, {
    name: 'UBN',
    elementarySectors: ['UB', 'UN'],
    canAccept: [],
    canGive: ['UB', 'UN'],
  }, {
    name: 'UXR',
    elementarySectors: ['UR', 'XR'],
    canAccept: ['KR', 'KHYR', 'UBN'],
    canGive: ['UR', 'XR'],
  }, {
    name: 'UXKR',
    elementarySectors: ['UR', 'XR', 'KR'],
    canAccept: ['HYR'],
    canGive: ['UXR', 'KR'],
  }, {
    name: '5R',
    elementarySectors: ['UR', 'XR', 'KR', 'HYR'],
    canAccept: [],
    canGive: ['UXR', 'KHYR', 'HYR'],
  }, {
    name: 'KHYR',
    elementarySectors: ['KR', 'HYR'],
    canAccept: ['UXR'],
    canGive: ['KR', 'HYR'],
  },
];

// For some reason, Jest expect a test in each file ...
test(() => {});
