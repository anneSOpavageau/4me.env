import getSectorsAPI from '../../../src/sectors';
import { getSectorsInCommon } from '../../../src/sectors/prettyName';

import { clusters, sectorGroups } from './mock';

describe('prettyName', () => {
  describe('getSectorsInCommon', () => {
    test('should return -1 when supplied sector group is larger than supplied ES', () => {
      const elementarySectors = ['UR', 'XR'];
      const sectorGroup = {
        name: 'UXKR',
        elementarySectors: ['UR', 'XR', 'KR'],
      };

      expect(getSectorsInCommon(elementarySectors, sectorGroup)).toBe(-1);
    });

    test('should return the number of sectors in common', () => {
      const sectorGroup = {
        name: '5R',
        elementarySectors: ['UR', 'XR', 'KR'],
      };

      expect(getSectorsInCommon(['UR', 'XR', 'KR', 'HYR'], sectorGroup)).toBe(3);
    });
  });

  const {
    getSectorGroupByName,
    getSectorGroupByElementarySectors,
    prettyName,
  } = getSectorsAPI({clusters, sectorGroups});

  test('should return a sector group name', () => {
    expect(prettyName(['UR', 'XR'])).toBe('UXR');
    expect(prettyName()).toBe('');
    expect(prettyName(null)).toBe('');
  });

  test('should reject non existent elementary sectors', () => {
    expect(() => prettyName(['UR', 'ZS'])).toThrow(/argument/i);
  });

  test('should return a sector group name when the sector group does not exist', () => {
    expect(prettyName(['UR', 'UB'])).toBe('UR,UB');
  });

  test('should accept a separator argument', () => {
    expect(prettyName(['UR', 'UB'], '+')).toBe('UR+UB');
  });

  test('should factorize description when the sector group does not exist', () => {
    expect(prettyName(['UR', 'XR', 'UB'])).toBe('UXR,UB');
    expect(prettyName(['UR', 'XR', 'KR', 'HYR', 'UN', 'E'])).toBe('5R,UN,E');
  });

  test('should handle recursion', () => {
    expect(prettyName(['UR', 'XR', 'KR', 'HYR', 'UB', 'UN', 'E'])).toBe('5R,UBN,E');
  });
});
