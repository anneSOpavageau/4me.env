import getSectorsAPI from '../../../src/sectors';
import R from 'ramda';

import { clusters, sectorGroups } from './mock';

describe('clusters', () => {
  const {
    getClusters,
    getElementarySectors,
  } = getSectorsAPI({clusters, sectorGroups});

  describe('getElementarySectors', () => {
    test('should return a flat array', () => {
      const elementarySectors = getElementarySectors();
      expect(Array.isArray(elementarySectors)).toBe(true);
      elementarySectors.forEach(sector => expect(typeof sector).toBe('string'));
    });
  });
});
