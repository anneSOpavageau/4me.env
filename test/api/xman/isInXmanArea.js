import getXmanAPI from '../../../src/xman';

import { xman } from './mock';
import { clusters } from '../sectors/mock';

const {
  isTracked,
  isCaptured,
  isFrozen,
  isInSector,
  isInGeoSector,
} = getXmanAPI({xman, clusters});

const inTrack = {
  altitude: 10000,
  lat: 0.5,
  long: 0.5,
};

const inCapture = {
  altitude: 10000,
  lat: 1.5,
  long: 1.5,
};

const inFreeze = {
  altitude: 10000,
  lat: 2.5,
  long: 2.5,
};

const inUR = {
  altitude: 31500,
  lat: 0.5,
  long: 0.5,
};

describe('xman areas', () => {
  test('should reject invalid destinations', () => {
    expect(() => isCaptured('NONEXISTENT', inCapture)).toThrow(/destination/i);
  });

  test('a captured flight should be tracked', () => {
    expect(isCaptured('EGLL', inCapture)).toBe(true);
    expect(isTracked('EGLL', inCapture)).toBe(true);
  });

  test('a frozen flight should be captured and tracked', () => {
    expect(isFrozen('EGLL', inFreeze)).toBe(true);
    expect(isCaptured('EGLL', inFreeze)).toBe(true);
    expect(isTracked('EGLL', inFreeze)).toBe(true);
  });

  test('should not explode when areas are not defined', () => {
    expect(() => isCaptured('LSZH', inFreeze)).not.toThrow();
  });
});

describe('isInSector', () => {
  test('should reject invalid destinations', () => {
    expect(() => isInSector('NONEXISTENT', 'UR', inUR)).toThrow(/destination/i);
  });

  test('should reject invalid sectors', () => {
    expect(() => isInSector('EGLL', 'NONEXISTENT', inUR)).toThrow(/sector/i);
  });

  test('should reject invalid flight positions', () => {
    const invalid = {
      lat: 'string',
      long: 0.5,
      altitude: 'string',
    };

    expect(() => isInSector('EGLL', 'UR', null)).toThrow(/position/i);
    expect(() => isInSector('EGLL', 'UR', invalid)).toThrow(/position/i);
  });

  test('should not explode when areas are not defined', () => {
    expect(() => isInSector('LSZH', 'UR', inFreeze)).not.toThrow();
  });

  test('should not explode when lat and long are 0', () => {
    const position = {
      lat: 0,
      long: 0,
      altitude: 10000,
    };

    expect(() => isInSector('EGLL', 'UR', position)).not.toThrow();
  });

  test('should return false when a flight is geographically outside a sector', () => {
    const outsideUR = {
      ...inUR,
      long: 5,
    };

    expect(isInSector('EGLL', 'UR', outsideUR)).toBe(false);
  });

  test('should return false when a flight is vertically outside a sector', () => {
    const outsideUR = {
      ...inUR,
      altitude: 0,
    };

    expect(isInSector('EGLL', 'UR', outsideUR)).toBe(false);
  });
});

describe('isInGeoSector', () => {
  test('should reject invalid destinations', () => {
    expect(() => isInGeoSector('NONEXISTENT', 'UR', inUR)).toThrow(/destination/i);
  });

  test('should reject invalid sectors', () => {
    expect(() => isInGeoSector('EGLL', 'NONEXISTENT', inUR)).toThrow(/sector/i);
  });

  test('should reject invalid flight positions', () => {
    const invalid = {
      lat: 'string',
      long: 0.5,
      altitude: 'string',
    };

    expect(() => isInGeoSector('EGLL', 'UR', null)).toThrow(/position/i);
    expect(() => isInGeoSector('EGLL', 'UR', invalid)).toThrow(/position/i);
  });

  test('should not explode when areas are not defined', () => {
    expect(() => isInGeoSector('LSZH', 'UR', inFreeze)).not.toThrow();
  });

  test('should return false when a flight is geographically outside a sector', () => {
    const outsideUR = {
      ...inUR,
      long: 5,
    };

    expect(isInGeoSector('EGLL', 'UR', outsideUR)).toBe(false);
  });

  test('should return true when a flight is vertically outside a sector', () => {
    const outsideUR = {
      ...inUR,
      altitude: 0,
    };

    expect(isInGeoSector('EGLL', 'UR', outsideUR)).toBe(true);
  });
});
