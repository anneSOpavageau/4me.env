import { isValidFlightPosition } from '../../../../src/utils/isInAirspace';
import R from 'ramda';

const VALID_FLIGHT_POSITION = {
  altitude: 5000,
  lat: 0.5,
  long: 0.5,
};


const INVALID_FLIGHT_POSITIONS = [
  undefined,
  null,
  'string',
  123,
  {...VALID_FLIGHT_POSITION, altitude: 'string'},
  {...VALID_FLIGHT_POSITION, lat: null},
  {...VALID_FLIGHT_POSITION, long: []},
];


describe('isValidFlightPosition', () => {
  test('should reject invalid flight positions', () => {
    INVALID_FLIGHT_POSITIONS.forEach(invalid => {
      expect(() => isValidFlightPosition(invalid)).toThrow(/invalid flight position/i);
    });
  });

  test('should accept valid flight positions', () => {
    expect(() => isValidFlightPosition(VALID_FLIGHT_POSITION)).not.toThrow();
    expect(isValidFlightPosition(VALID_FLIGHT_POSITION)).toBe(true);
  });

  test('should accept positions with altitude set at 0', () => {
    const position = {
      ...VALID_FLIGHT_POSITION,
      altitude: 0,
    };

    expect(() => isValidFlightPosition(position)).not.toThrow();
    expect(isValidFlightPosition(position)).toBe(true);
  });

});
