import { isInAirspace } from '../../../../src/utils/isInAirspace';
import R from 'ramda';

const VALID_POSITION = {
  altitude: 5000,
  lat: 0.5,
  long: 0.5,
};

const VALID_AIRSPACE_SLICES = [
  {
    altitude: {
      min: 0,
      max: 26500,
    },
    polygon: [
      [0, 0],
      [1, 0],
      [1, 1],
      [0, 1],
      [0, 0],
    ],
  }, {
    altitude: {
      min: 26500,
      max: 31500,
    },
    polygon: [
      [0, 0],
      [1, 0],
      [1, 1],
      [0, 1],
      [0, 0],
    ],
  }, {
    altitude: {
      min: 26500,
      max: 31500,
    },
    polygon: [
      [2, 2],
      [3, 2],
      [3, 3],
      [2, 3],
      [2, 2],
    ],
  }
];

const VALID_AIRSPACE = [
  VALID_AIRSPACE_SLICES[0],
];


describe('isInAirspace', () => {
  // These are just smoke tests
  // Data validation is more thouroughly tested separately
  test('should reject invalid airspace data', () => {
    expect(() => isInAirspace(null, VALID_POSITION)).toThrow();
  });

  test('should reject invalid position data', () => {
    expect(() => isInAirspace(VALID_AIRSPACE, null)).toThrow();
  });

  test('should accept valid data', () => {
    expect(() => isInAirspace(VALID_AIRSPACE, VALID_POSITION)).not.toThrow();
  });

  test('should return true when a flight is in an airspace', () => {
    expect(isInAirspace(VALID_AIRSPACE, VALID_POSITION)).toBe(true);
  });

  test('should return false when a flight is geographically outside an airspace', () => {
    const outside = {
      ...VALID_POSITION,
      lat: 2,
    };

    expect(isInAirspace(VALID_AIRSPACE, outside)).toBe(false);
  });

  test('should return false when a flight is vertically outside an airspace', () => {
    const outside = {
      ...VALID_POSITION,
      altitude: 31000,
    };

    expect(isInAirspace(VALID_AIRSPACE, outside)).toBe(false);
  });

  test('should test multiple altitude ranges', () => {
    const airspace = [
      VALID_AIRSPACE_SLICES[0],
      VALID_AIRSPACE_SLICES[1],
    ];

    const outside = {
      ...VALID_POSITION,
      altitude: 38000,
    };

    expect(isInAirspace(airspace, VALID_POSITION)).toBe(true);
    expect(isInAirspace(airspace, outside)).toBe(false);
  });

  test('should test multiple geographical definitions', () => {
    const airspace = [
      VALID_AIRSPACE_SLICES[0],
      VALID_AIRSPACE_SLICES[2],
    ];

    const outside = {
      lat: 1.5,
      long: 1.5,
      altitude: 26500,
    };

    const inside = {
      lat: 2.5,
      long: 2.5,
      altitude: 31000,
    };

    expect(isInAirspace(airspace, outside)).toBe(false);
    expect(isInAirspace(airspace, inside)).toBe(true);

  });

});
