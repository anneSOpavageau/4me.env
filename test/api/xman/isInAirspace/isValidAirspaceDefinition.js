import { isValidAirspaceDefinition } from '../../../../src/utils/isInAirspace';
import R from 'ramda';

const VALID_AIRSPACE_SLICES = [
  {
    altitude: {
      min: 0,
      max: 26500,
    },
    polygon: [
      [0, 0],
      [1, 0],
      [1, 1],
      [0, 1],
      [0, 0],
    ],
  },
];

const VALID_AIRSPACES = [
  [
    VALID_AIRSPACE_SLICES[0],
  ],
];


const INVALID_AIRSPACES = [
  undefined,
  null,
  'string',
  123,
  [
    {foo: 'bar'},
    R.clone(VALID_AIRSPACE_SLICES[0]),
  ],
  [
    R.clone(VALID_AIRSPACE_SLICES[0]),
    {
      altitude: {min: 'string'},
      polygon: R.clone(VALID_AIRSPACE_SLICES[0].polygon),
    },
  ],
  [
    R.clone(VALID_AIRSPACE_SLICES[0]),
    {
      altitude: {min: 0, max: 66000},
      polygon: [
        [0, 0],
        [1, 0],
        [1, 1],
        [0, 1],
      ],
    },
  ],
];


describe('isValidAirspaceDefinition', () => {
  test('should reject invalid airspace data', () => {
    INVALID_AIRSPACES.forEach(invalid => {
      expect(() => isValidAirspaceDefinition(invalid)).toThrow(/invalid airspace definition/i);
    });
  });

  test('should accept valid airspace data', () => {
    VALID_AIRSPACES.forEach(valid => {
      expect(() => isValidAirspaceDefinition(valid)).not.toThrow();
      expect(isValidAirspaceDefinition(valid)).toBe(true);
    });
  });

});
