export const clients = [
  {id: 1, type: 'flow-manager', name: 'FMP'},
  {
    id: 2,
    type: 'cwp',
    name: 'P2',
    backupedRadios: [],
    suggestions: {
      filteredSectors: ['5R'],
      preferenceOrder: ['UBN'],
    },
  }, {
    id: 3,
    type: 'cwp',
    name: 'P3',
    backupedRadios: [],
    suggestions: {
      filteredSectors: ['UBN'],
      preferenceOrder: ['KHYR', 'UXR'],
    },
  }, {
    id: 4,
    type: 'cwp',
    name: 'P4',
    backupedRadios: [],
    suggestions: {
      filteredSectors: ['E', 'SE'],
      preferenceOrder: ['UBN'],
    },
  }, {
    id: 5,
    type: 'cwp',
    name: 'P5',
    backupedRadios: [],
    suggestions: {
      filteredSectors: [],
      preferenceOrder: [],
    },
  }, {
    id: 6,
    type: 'cwp',
    name: 'P6',
    backupedRadios: [],
    suggestions: {
      filteredSectors: ['5R'],
      preferenceOrder: [],
    },
  }, {
    id: 7,
    type: 'cwp',
    name: 'P7',
    backupedRadios: [],
    suggestions: {
      filteredSectors: ['E', 'SE'],
      preferenceOrder: ['5R', 'UBN'],
    },
  },
];

export const map = [
  {clientId: 2, sectors: ['KR', 'HYR']},
  {clientId: 3, sectors: ['UN', 'UB', 'UR', 'XR']},
  {clientId: 4, sectors: ['E', 'SE']},
];

test(() => {});
