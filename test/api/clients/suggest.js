import getSectorSuggestions from '../../../src/clients/getSectorSuggestions';
import { clients, map } from './mock';
import { sectorGroups, clusters } from '../sectors/mock';

const data = {
  clients,
  sectorGroups,
  clusters,
};

import R from 'ramda';

describe('suggestion engine', () => {
  const getSuggestions = getSectorSuggestions.bind(null, data);
  describe('input validation', () => {
    test('should reject invalid arguments', () => {
      expect(() => getSuggestions()).toThrow(/argument/i);
      expect(() => getSuggestions('string')).toThrow(/argument/i);
      expect(() => getSuggestions(['a', 'b'])).toThrow(/argument/i);
    });

    test('should reject invalid clientId', () => {
      expect(() => getSuggestions(map)).toThrow(/argument/i);
      expect(() => getSuggestions(map, 'string')).toThrow(/argument/i);
    });

    test('should reject invalid clientId in map', () => {
      const invalidMap = [
        {clientId: 'unknown', sectors: ['UR', 'XR']},
      ];
      expect(() => getSuggestions(invalidMap, 2)).toThrow(/argument/i);
    });

    test('should reject undefined clientId in map', () => {
      const invalidMap = [
        {sectors: ['UR', 'XR']},
      ];
      expect(() => getSuggestions(invalidMap, 2)).toThrow(/argument/i);
    });

    test('should reject unknown sectors', () => {
      const invalidMap = [
        {clientId: 3, sectors: ['UR', 'XR', 'UNKNOWN']},
      ];
      expect(() => getSuggestions(invalidMap, 2)).toThrow(/argument/i);
    });

    test('should accept map with added metadata', () => {
      const validMap = [
        {clientId: 2, disabled: true},
        {clientId: 3, sectors: ['UR', 'XR']},
      ];

      expect(() => getSuggestions(validMap, 2)).not.toThrow();

    });
  });

  describe('on empty sectors', () => {
    test('should collect canGive from all bound sectors', () => {
      const suggestions = getSuggestions(map, 5);

      // Here we expect every canGive of the room
      // Plus every elementary sector combination already bound in the room
      // For example, CWP #2 has ['KR', 'HYR'] bound,
      // We expect ['KR'] and ['HYR'] to be suggested as they are ['KR', 'HYR'].canGive
      // We also expect ['KR', 'HYR']
      //
      const expected = [
        // CWP #2
        ['KR', 'HYR'],
        ['KR'],
        ['HYR'],
        // CWP #3
        ['UR', 'XR', 'UN', 'UB'],
        ['UR', 'XR'],
        ['UB', 'UN'],
        // CWP #4
        ['E', 'SE'],
      ];

      expect(suggestions).toMatchSortedSuggestions(expected);
    });

    test('should reject filtered sector groups', () => {
      const suggestions = getSuggestions(map, 6);

      const expected = [
        // CWP #2 contains only 5R sectors, nothing should appear here
        // CWP #3
        ['UB', 'UN'],
        // CWP #4
        ['SE', 'E'],
      ];

      expect(suggestions).toMatchSortedSuggestions(expected);
    });

    test('should take into account preference order', () => {
      const suggestions = getSuggestions(map, 7);
      const expected = [
        // 5R
        ['KR', 'HYR'],
        ['UR', 'XR'],
        ['KR'],
        ['HYR'],
        // UBN
        ['UB', 'UN'],
        // Rest
        ['UR', 'XR', 'UN', 'UB'],
        // 'E' and 'SE' are filtered
        // ['E', 'SE'],
      ];

      expect(suggestions).toMatchSortedSuggestions(expected);
    });
  });

  describe('on bound sectors', () => {
    test('should return canAccept', () => {
      const suggestions = getSuggestions(map, 2);
      const expected = [
        // We have KHYR bound, KHYR can accept UXR, we should see 5R suggested
        ['UR', 'XR', 'KR', 'HYR'],
      ];

      expect(suggestions).toMatchSortedSuggestions(expected);
    });

    test('should return no suggestions on unknown sector group', () => {
      const suggestions = getSuggestions(map, 4);
      const expected = [];

      expect(suggestions).toMatchSortedSuggestions(expected);
    });
  });
});
