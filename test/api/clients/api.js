import getClientsAPI from '../../../src/clients';

import { clients } from './mock';
import { sectorGroups, clusters } from '../sectors/mock';

const expectedApi = {
  getClients: 'function',
  getSectorSuggestions: 'function',
};

describe('getClientsAPI', () => {
  test('should reject invalid data', () => {
    expect(() => getClientsAPI()).toThrow();
    expect(() => getClientsAPI('string')).toThrow();
    expect(() => getClientsAPI({})).toThrow();
  });

  const API = getClientsAPI({clients, clusters});

  test('should return an object', () => {
    expect(typeof API).toBe('object');
  });

  Object.keys(expectedApi).forEach(apiMethod => {
    describe(`${apiMethod}`, () => {
      test('should exist', () => {
        expect(API[apiMethod]).toBeDefined();
      });

      test(`should be a ${expectedApi[apiMethod]}`, () => {
        expect(typeof API[apiMethod]).toBe(expectedApi[apiMethod]);
      });
    });
  });
});
