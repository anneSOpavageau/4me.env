import React from 'react';
import { storiesOf, action } from '@kadira/storybook';

import getEnv, { ENVIRONMENTS } from '../src';

const knownEnvironments = Object.keys(ENVIRONMENTS);

const SmallRoundCircle = ({color = '#3F51B5', children}) => (
  <div style={{
    width: 28,
    height: 28,
    borderRadius: '50%',
    backgroundColor: color,
    color: '#FFF',
    lineHeight: "28px",
    textAlign: 'center',
    margin: 5,
  }}>
    {children}
  </div>
);

const LargeRoundCircle = ({color = '#3F51B5', children}) => (
  <div style={{
    width: 100,
    height: 100,
    borderRadius: '50%',
    backgroundColor: color,
    color: '#FFF',
    lineHeight: "100px",
    textAlign: 'center',
    margin: 5,
  }}>
    {children}
  </div>
);


const SmallCwpButton = ({clientId}) => (
  <SmallRoundCircle>{clientId}</SmallRoundCircle>
);

const LargeCwpButton = ({clientId}) => (
  <LargeRoundCircle>{clientId}</LargeRoundCircle>
);


knownEnvironments.forEach(envId => {
  const env = getEnv(envId);

  const ControlRoomLayout = env.components.getControlRoomLayout();

  storiesOf(`${envId}.ControlRoomLayout`, module)
    .add('Small version', () => {
      return (
          <ControlRoomLayout
            cwpButton={<SmallCwpButton/>}
          />
      );
    })
    .add('Small version with RoomStatus', () => {
      const RoomStatus = () => {
        return (
          <SmallRoundCircle color="red" />
        );
      }

      return (
        <ControlRoomLayout
          cwpButton={<SmallCwpButton />}
          roomStatus={<RoomStatus />}
        />
      );
    })
    .add('Large version with 100px wide RoomStatus', () => (
      <ControlRoomLayout
        cwpButton={<LargeCwpButton />}
        roomStatus={<div style={{width: 100}} />}
      />
    ));
});
