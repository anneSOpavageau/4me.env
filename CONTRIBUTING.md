# Contributing

## Setup your dev environment
### Setup a local dev environment
This part assumes the following dependencies are installed on your local machine :
* nodejs v7
* yarnpkg

* Install dependencies
```
# yarn
```


* If everything checks out, submit a merge request

### Use vagrant to setup a virtual dev environment
This part assumes the following dependencies are installed on your local machine :
* vagrant
* a vagrant provider (tested with virtualbox)

* Clone this repo or your fork
* Use vagrant to spin up a development VM (this will take some time)
```
# vagrant up
```

Vagrant takes care of spinning and configuring the virtual machine. All the files in your local folder will be synced with what's inside the VM.

* Login inside the VM
```
# vagrant ssh
```

You should now have a shell access, in a virtual machine, with all project files available.

* Install JavaScript dependencies
```
ubuntu@4me-env:/vagrant$ yarn
```

> **Note about storybook:**
> You can run storybook inside the virtual machine using `npm run storybook`
> Storybook exposes a webserver on port 9001 inside the VM. Vagrant is configured to allow external connections on port 9001.
> This means you can run `npm run storybook` inside the VM and still access it from outside using your web browser and poiting it to `http://localhost:9001`

## Check your changes
* Fork this repository, clone your fork locally, and setup your environment accordingly

* Make your changes

* Run static type checker
```
# npm run flow
```

* Run unit tests
```
# npm t
```

## Add a different environment
* Create environment data in `src/data`. You can base your work on `LFEE` env and build from there.
* Import your env in `src/index.js`

```JavaScript
// Import data
import LFEE from './data/LFEE';
import YOURENV from './data/YOURENV';
```

* Add it to the `ENVIRONMENTS` const

```JavaScript
export const ENVIRONMENTS = {
  LFEE,
  YOURENV,
};
```

* Use [FlowType][flow_type] to check for data structure consistency :

```
# npm run flow
[...]

No errors!
```

* Check your env is valid with a series of unit tests

```
# npm t
[...]

Test Suites: 3 skipped, 11 passed, 11 of 14 total
Tests:       9 skipped, 988 passed, 997 total
Snapshots:   0 total
Time:        2.817s
Ran all test suites.
```

### FlowType and static typing
As you may already know, JavaScript is a loose typed language. [FlowType][flow_type] (like TypeScript) is a static type checker for JavaScript. Using [FlowType][flow_type] in this codebase brings type safety to this library. [FlowType][flow_type] is able to catch errors that would require extensive unit tests to uncover.

In this library, [FlowType][flow_type] is used to enforce data structure consistency.

Types are defined in [src/types/index.flow.js](src/types/index.flow.js). For example :

```JavaScript
export type SectorGroup = {|
  name: string,
  elementarySectors: Array<ElementarySector>,
  canAccept: Array<ElementarySector>,
  canGive: Array<ElementarySector>,
|};

export type ElementarySector = string;
```
We defined two types here. `ElementarySector` which is a string and `SectorGroup` which is an object type.

A `SectorGroup` must have these properties set : `name`, `elementarySectors`, `canAccept`, `canGive`.

Likewise, these properties must have a specific type.

Long story short, if I were to define a sectorGroup in my environment data like so :

```JavaScript
const exampleSector = {
  name: 2, // SectorGroup name should be a string !
  elementarySectors:["UE"],
  canAccept:[
    "XE",
    "XKHE"
  ],
  canGive:[]
};
```

Flow would raise an error :

```
# npm run flow

src/data/LFEE/sector_groups/4E.js:4
  4:     "name": 2,
                 ^ number. This type is incompatible with
 25:   name: string,
             ^^^^^^ string. See: src/types/index.js.flow:25


Found 1 error
```

Type checking is already set up in this repo. The only thing you must do in order to benefit from it is to add a single line at the top of your data definitions :

```JavaScript
// @flow
```
This indicates to Flow you want this specific file typechecked.

### Unit tests
Some parts of the env reference other parts. Unit tests enforce consistency.

For instance, let's say I defined a cluster referencing an unknown elementary sector. Unit tests will catch this and spit out an error.
```
[...]
 FAIL  test/data/sectorGroups.js
  ● sector group data › LFEE › ZS should be defined in sector groups
[...]
```

Unit tests will also output warnings of some data seems missing.
```
[...]
    console.warn test/data/xman.js:61
      LFEE/XMAN/LSZH: No track area defined !
[...]
```

In this case, the scripts indicate that `LSZH` XMAN destination in `LFEE` environment misses a `track` area.

### ControlRoomLayout
Defining your data for your environment should be fairly straightforward provided a basic knowledge of JavaScript data structures.

Defining your own ControlRoomLayout will require a bit more work and knowledge about React and React components.

The `<ControlRoomLayout />` component in `LFEE` environment has lots of comments in the code. This should be a nice place to start.

#### Using StoryBook to preview your ControlRoomLayout component
The `<ControlRoomLayout />` component is rendered in 4me.frontend application. To make it easier to tweak your layout, this project uses [React StoryBook](https://github.com/storybooks/react-storybook) to make it easier to see your changes without having to bootstrap a full 4me.frontend application (see issue devteamreims/4ME#175).

Simply run `npm run storybook` and point your browser to `localhost:9001`.

Storybook provides hot reloading. Editing your file and saving changes should show changes directly in your browser window without refreshing the page.

[flow_type]: https://flowtype.org
