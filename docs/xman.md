# 4me.env `xman` API

## Datatypes
See [types/index.js](types/index.js) and [types/xman.js](types/xman.js)

### Generic geographic types

#### GeoPoint
Represents a single point in GeoJSON coordinates format.

Coordinates are decimal, in this order : `[long, lat]`;

#### AirspaceSlice (Object)
Represents a slice of airspace.  
Properties:
* `polygon`: An array of `GeoPoints` forming an area. The polygon must be closed, meaning the last item of the array of coordinates must match the first item.
* `altitude` (optional): Object representing vertical limits of defined airspace slice.
* `altitude.min` (optional): Minimum altitude in feet. Defaults to `-Infinity`.
* `altitude.max` (optional): Maximum altitude in feet. Defaults to `Infinity`.

#### AirspaceDefinition (Array)
An array of `AirspaceSlices` forming a full airspace.

#### FlightPosition (Object)
Represents the position of a flight.  
Properties:
* `lat`: (number) Lattitude expressed in decimal notation.
* `long`: (number) Longitude expressed in decimal notation.
* `altitude`: (number) Altitude expressed in feet.

### XMAN Specific types

#### DestinationId (string)
XMAN destination identifier. e.g. `EGLL`

#### Destination (Object)
Represents a valid XMAN destination.  
Properties:
* `name`: DestinationId (e.g. `EGLL`)
* `displayName`: string representing a human friendly name for the XMAN destination
* `mode`: Either `speed` or `mach`. Informing consuming apps of the specific XMAN mode selected for this destination.

#### XmanAreas (Object)
Object holding all geographical data for a specific destination.  
Properties:
* `track` (AirspaceDefinition): Airspace in which flights towards this destination are considered tracked.
* `capture` (AirspaceDefinition): Airspace in which flights are considered captured.
* `freeze` (AirspaceDefinition): Airspace in which flights are considered frozen.
* `sectors` (Object): Object keyed by `ElementarySectors` with `AirspaceDefinition` as values. Each `AirspaceDefinition` represents the XMAN interest area for a specific `ElementarySector` for a particular `DestinationId`.

## Note about global areas
Our XMAN implementation defines 3 areas.

The first one is the `track` area. All XMAN flights physically outside this area should be ignored by our system.

The second one is the `capture` area. All XMAN flights outside this area should not generate a speed or mach reduction advisory. This area is there to model XMAN horizons.
> A flight in the `capture` area is automatically considered in the `track` area.

The third one is the `freeze` area. All XMAN flights inside this area should see their speed or mach reduction advisory frozen until they leave the `freeze` area.
> A flight in the `freeze` area is considered in the `track` and `capture` areas.

## API
### getDestinations
> :: _ -> [Destination]

Get a list of all XMAN destinations defined in the environment.

### getAreasForDestination
> :: DestinationId -> XmanAreas  

Takes a `DestinationId` as argument and return `XmanAreas` for this specific destination in the environment.

### isTracked, isCaptured, isFrozen
> :: (DestinationId, FlightPosition) -> boolean

Returns true if a given flight position is inside the `track`, `capture` or `freeze` area for a specific destination in the environment.

Returns false otherwise.

Throws if `DestinationId` is unknown in the environment or if `FlightPosition` is malformed.

### isInSector, isInGeoSector
> :: (DestinationId, ElementarySector, FlightPosition) -> boolean

Returns true if a given flight position is inside the area of interest of provided `ElementarySector`, for provided `DestinationId` in the environment. `isInSector` will check altitude. `isInGeoSector` will only check the geographical position, discarding altitude.

Returns false otherwise.

Throws if `DestinationId` or `ElementarySector` is unknown in the environment or if `FlightPosition` is malformed.
