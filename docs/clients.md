# 4me.env `clients` API

## Datatypes
See [types/index.js](types/index.js).

### Client
Represents a 4ME client.

## API
### getClients
> :: _ -> [Client]

Retrieve the full list of 4ME clients defined in the environment.

### getClientById
> :: ClientId -> Client|null

Retrieve a single client. Returns `null` if the client is not found.

### getSuggestions
> :: (Map, ClientId) -> [[ElementarySector]]

Takes a `map` and a `clientId` and return an array of suggested elementary sectors which could be bound to this particular client given the state of the room.

`map` shape :
```JavaScript
const map = [
  {clientId: 2, sectors: ['UR', 'XR']},
  {clientId: 3, sectors: ['KR', 'HYR']},
];
```
