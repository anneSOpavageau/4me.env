# 4me.env `components` API

This sub api is dedicated to environment specific react components. The main use for this API is in `4me.frontend`.

> **IMPORTANT NOTE**: This library lists `react` as a peer dependency. However, the `components` API relies on `react`. When `react` is not reachable, this part of the API will not exist. Therefore, you can use this library on the backend without injecting a dependency to `react`.

## Datatypes
See [types/index.js](types/index.js).

## API
### getControlRoomLayout
> :: _ -> ReactComponent

Returns a <ControlRoomLayout /> component whose job is to render a layout of the control room in the selected environment.

#### Expected props
* cwpButton (ReactElement): A ReactElement which will be rendered with a `clientId` prop injected.
* roomStatus (ReactElement / optional): A single ReactElement that will be rendered in a defined place, used to provide additional information about the control room.

#### Examples
![full_size_layout][full_size_layout]

Circled in green is a `cwpButton`. Circled in red is `roomStatus`.

![mini_layout][mini_layout]


[full_size_layout]: ./images/ControlRoomLayout.png
[mini_layout]: ./images/ControlRoomLayout-mini.png
