# 4me.env `sectors` API

## Datatypes
See [types/index.js](types/index.js).

### ElementarySector
Represents an elementary sector.

### SectorGroup
Represents a sector group (composed of one or more elementary sectors).

### SectorBlock
Represents an association of elementary sectors composed to represent an ATC sector block.  
For example, in `LFEE` environment, `UR`, `XR`, `KR`, `HYR` represent a sector block (namely `5R`).

### Cluster
Represents a group of SectorBlocks forming an ATC cluster.

## API
### getElementarySectors
> :: _ -> [ElementarySector]

Get a list of all elementary sectors defined in the environment.

### getClusters
> :: _ -> [Cluster]  
> :: _ -> [[SectorBlock]]  
> :: _ -> [[[ElementarySector]]]

Get a list of all clusters defined in the environment.  

### getSectorGroups
> :: _ -> [SectorGroup]

Get a list of all sector groups defined in the environment.

### getSectorGroupByName
> :: String -> SectorGroup | undefined

Get a specific SectorGroup identified by its name.  
Returns `undefined` if the SectorGroup doesn't exist.

### getSectorGroupByElementarySectors
> :: [ElementarySector] -> SectorGroup | undefined

Get a specific SectorGroup identified by its elementary sectors.  
Returns `undefined` if the SectorGroup doesn't exist.

### prettyName
> :: [ElementarySector] -> String
> :: ([ElementarySector], String) -> String

Takes a set of elementary sectors and return the canonical name associated as defined in the environment.  
When there are no sector groups associated with provided elementary sectors, this function will return a string expressing what could be a common name for the provided elementary sectors.  
This function takes an optional second argument `separator` used when computing a non-existent sector group name.  
`separator` default value is `,`

```JavaScript
prettyName(['UE', 'XE']); // "UXE"
// Non existent sector group
prettyName(['UE', 'XH']); // "UE,XH"
// Likewise but with `separator`
prettyName(['UE', 'XH'], '+'); // "UE+XH"
```
