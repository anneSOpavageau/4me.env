// @flow

import type {
  EnvironmentData,
  ControlRoomLayout,
} from '../types';

export default function getControlRoomLayout(data: EnvironmentData): ControlRoomLayout {
  if(!data.components) {
    throw new Error('this should never happen !');
  }

  return data.components.ControlRoomLayout;
}
