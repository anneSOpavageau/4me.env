// @flow
import R from 'ramda';
import invariant from 'invariant';

import type {
  EnvironmentData,
  Frequency,
} from '../types';

export type FrequencyAPI = {|
  getFrequencies: (void) => Array<Frequency>,
|};

import getFrequencies from './getFrequencies';

const getFrequenciesAPI = (data: EnvironmentData): FrequencyAPI => {
  invariant(
    data,
    'Invalid data provided'
  );

  const {
    frequencies,
  } = data;

  invariant(
    frequencies,
    'Invalid data provided',
  );

  return {
    getFrequencies: getFrequencies.bind(null, data),
  };
};

export default getFrequenciesAPI;
