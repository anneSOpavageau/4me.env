// @flow

import type {
  EnvironmentData,
  Frequency,
} from '../types';

export default function getFrequencies(data: EnvironmentData): Array<Frequency> {
  return data.frequencies;
}
