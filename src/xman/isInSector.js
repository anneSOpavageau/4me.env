// @flow
import type {
  EnvironmentData,
  FlightPosition,
  ElementarySector,
} from '../types';

import type {
  DestinationId,
} from '../types/xman';

import R from 'ramda';
import invariant from 'invariant';

import { isInAirspace } from '../utils/isInAirspace';

import getAreasForDestination from './getAreasForDestination';
import getElementarySectors from '../sectors/getElementarySectors';

export const isInSector = (
  data: EnvironmentData,
  destination: DestinationId,
  sector: ElementarySector,
  coords: FlightPosition,
): bool => {
  const areas = getAreasForDestination(data, destination);
  if(!areas.sectors) {
    return false;
  }

  const sectors = getElementarySectors(data);

  invariant(
    sectors && sectors.includes(sector),
    `${sector} is not a valid sector`
  );

  const area = areas.sectors[sector];

  // This sector has no interest area defined, the flight is not inside it
  if(!area) {
    return false;
  }


  return isInAirspace(area, coords, true);
};

export const isInGeoSector = (
  data: EnvironmentData,
  destination: DestinationId,
  sector: ElementarySector,
  coords: FlightPosition,
): bool => {
  const areas = getAreasForDestination(data, destination);
  if(!areas.sectors) {
    return false;
  }

  const sectors = getElementarySectors(data);

  invariant(
    sectors && sectors.includes(sector),
    `${sector} is not a valid sector`
  );

  const area = areas.sectors[sector];

  // This sector has no interest area defined, the flight is not inside it
  if(!area) {
    return false;
  }


  return isInAirspace(area, coords, false);
};
