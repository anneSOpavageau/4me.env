import R from 'ramda';

import type {
  EnvironmentData,
} from '../types';

import type {
  Destination,
} from '../types/xman';

export default function getDestinations(
  data: EnvironmentData,
): Destination {
  return R.pipe(
    R.mapObjIndexed((val, name) => R.assoc('name', name, val)),
    R.values,
  )(data.xman.destinations);
}
