// @flow
import specials from './specials';
import zone1 from './zone1';
import zone2 from './zone2';
import zone3 from './zone3';

export default [
  ...specials,
  ...zone1,
  ...zone2,
  ...zone3,
];
