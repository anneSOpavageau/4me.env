// @flow
const zone3 = [
  {
    id: 30,
    name: 'P30',
    type: 'cwp',
    suggestions: {
      filteredSectors: [
        "KD2F",
        "4E",
        "4H"
      ],
      preferenceOrder: [
        "5R"
      ]
    },
    backupedRadios: [
      "135.505",
      "132.785",
      "126.285",
      "136.330",
    ]
  }, {
    id: 31,
    name: 'P31',
    type: 'cwp',
    suggestions: {
      filteredSectors: [
        "RFUE"
      ],
      preferenceOrder: [
        "5R"
      ]
    },
    backupedRadios: [
      "135.505",
      "132.785",
      "126.285",
    ]
  }, {
    id: 32,
    name: 'P32',
    type: 'cwp',
    suggestions: {
      filteredSectors: [
        "RFUE"
      ],
      preferenceOrder: [
        "5R"
      ]
    },
    backupedRadios: [
      "127.855",
      "132.505",
      "135.505",
      "132.785",
      "126.285",
      "136.330",
    ]
  }, {
    id: 33,
    name: 'P33',
    type: 'cwp',
    suggestions: {
      filteredSectors: [
        "RFUE"
      ],
      preferenceOrder: [
        "5R"
      ]
    },
    backupedRadios: [
      "135.505",
      "132.785",
      "126.285",
      "136.330",
    ]
  }, {
    id: 34,
    name: 'P34',
    type: 'cwp',
    suggestions: {
      filteredSectors: [
        "RFUE"
      ],
      preferenceOrder: [
        "4N"
      ]
    },
    backupedRadios: [
      "127.855",
      "132.505",
      "136.330",
    ]
  }, {
    id: 35,
    name: 'P35',
    type: 'cwp',
    suggestions: {
      filteredSectors: [
        "RFUE"
      ],
      preferenceOrder: [
        "4N"
      ]
    },
    backupedRadios: [
      "133.255",
      "133.005",
      "127.855",
      "132.505",
    ]
  }, {
    id: 36,
    name: 'P36',
    type: 'cwp',
    suggestions: {
      filteredSectors: [
        "RFUE"
      ],
      preferenceOrder: [
        "4N"
      ]
    },
    backupedRadios: [
      "133.255",
      "133.005",
      "127.855",
      "132.505",
    ]
  }, {
    id: 37,
    name: 'P37',
    type: 'cwp',
    suggestions: {
      filteredSectors: [
        "RFUE"
      ],
      preferenceOrder: [
        "4N"
      ]
    },
    backupedRadios: [
      "133.255",
      "133.005",
      "127.855",
      "132.505",
    ]
  }
];

export default zone3;
