// @flow
const frequencies = [
  // FIR
  {
    name: '128.300',
    defaultSector: 'E',
  },
  {
    name: '124.950',
    defaultSector: 'SE',
  },
  // 4H
  {
    name: '134.405',
    defaultSector: 'UH',
  },
  {
    name: '133.415',
    defaultSector: 'XH',
  },
  {
    name: '133.830',
    defaultSector: 'KH',
  },
  {
    name: '128.835',
    defaultSector: 'HH',
  },
  // 4E
  {
    name: '127.555',
    defaultSector: 'UE',
  },
  {
    name: '132.880',
    defaultSector: 'XE',
  },
  {
    name: '132.390',
    defaultSector: 'KE',
  },
  {
    name: '131.085',
    defaultSector: 'HE',
  },
  // 2F
  {
    name: '134.960',
    defaultSector: 'UF',
  },
  {
    name: '132.280',
    defaultSector: 'KF',
  },
  // KD
  {
    name: '135.305',
    defaultSector: 'KD',
  },
  // 5R
  {
    name: '136.330',
    defaultSector: 'HYR',
  },
  {
    name: '126.285',
    defaultSector: 'KR',
  },
  {
    name: '132.785',
    defaultSector: 'XR',
  },
  {
    name: '135.505',
    defaultSector: 'UR',
  },
  // 4N
  {
    name: '132.505',
    defaultSector: 'HN',
  },
  {
    name: '127.855',
    defaultSector: 'KN',
  },
  {
    name: '133.005',
    defaultSector: 'UB',
  },
  {
    name: '133.255',
    defaultSector: 'UN'
  },
  // Backup frequencies
  {
    name: '121.075',
    defaultSector: null,
  },
  {
    name: '132.630',
    defaultSector: null,
  },
  {
    name: '135.105',
    defaultSector: null,
  },
];

export default frequencies;
