// @flow
export const destinations = {
  'EGLL': {
    displayName: 'London Heathrow',
    mode: 'mach',
  },
  'LSZH': {
    displayName: 'Zurich',
    mode: 'speed',
  }
};

import type { SourceXmanData } from '../../../types/xman';

import EGLL from './EGLL';

const areas = {
  EGLL,
};

export default ({
  destinations,
  areas,
}: SourceXmanData);
