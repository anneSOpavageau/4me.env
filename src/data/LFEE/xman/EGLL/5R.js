// @flow

// XMAN LFEE EGLL [UR, XR, KR, HYR] definitions

const geo = [
  [3.5550684000000006, 50.4463304],
  [2.75, 49.45],
  [2.1533203, 49.23194730000001],
  [2.9186111, 48.2833333],
  [3.8507080000000005, 47.1299508],
  [6.015014600000001, 47.9605024],
  [7.064209, 49.1457836],
  [5.971069299999999, 49.460983899999995],
  [3.5550684000000006, 50.4463304],
];

export default {
  UR: [{
    altitude: {min: 26500, max: 31500},
    polygon: geo,
  }],
  XR: [{
    altitude: {min: 31500, max: 34500},
    polygon: geo,
  }],
  KR: [{
    altitude: {min: 34500, max: 36500},
    polygon: geo,
  }],
  HYR: [{
    altitude: {min: 36500, max: 66000},
    polygon: geo,
  }],
};
