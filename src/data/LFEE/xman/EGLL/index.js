// @flow

import { capture, freeze, track } from './global';

import KD from './KD';
import TWOF from './2F';
import FIVER from './5R';
import FOURN from './4N';

export default {
  capture,
  freeze,
  track,
  sectors: {
    ...KD,
    ...TWOF,
    ...FIVER,
    ...FOURN,
  },
};
