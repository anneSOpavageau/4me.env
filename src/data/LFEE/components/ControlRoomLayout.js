// @flow
import React, { Component } from 'react';

import Flexbox from 'flexbox-react';


// Helper components around flexbox-react
const Row = ({
  children,
  ...rest
} : {
  children?: React.Element<*>,
}): React.Element<*> =>
  React.createElement(Flexbox, {...rest, flexDirection: 'row'}, children);

const Column = ({
  children,
  ...rest
} : {
  children?: React.Element<*>,
}): React.Element<*> =>
  React.createElement(Flexbox, {...rest, flexDirection: 'column'}, children);


// This is an empty react component, just a function that returns null
const Empty = () => null;

// This is FlowType. We define an object type called Props that will be used in our component definition below
type Props = {
  cwpButton: React.Element<*>,
  // This prop is optional (see question mark)
  roomStatus?: React.Element<*>,
};

/**
 * This component controls the general layout of the control room
 * The idea is to allow parent to provide two sub components as props :
 * * A CwpButton, which will get a cwpId injected as prop
 * * An optional RoomStatus
 * The layout should be built using Flexbox
 * This allows the layout to be reused in different places where the container size changes
 * For instance, this layout could be used in a widget context or a modal context
 * This layout is used multiple contexts in `4me.frontend` :
 *   * Control room main page
 *   * Control room widget
 */
class ControlRoomLayout extends Component {
  // Here, we instruct flowtype that this component expects props matching the type Props
  props: Props;

  // Even though we use flowtype, we keep regular proptypes in place :
  // Users of this library might not have flowtype setup
  static propTypes = {
    cwpButton: React.PropTypes.element.isRequired,
    roomStatus: React.PropTypes.element,
  };

  // Since roomStatus is an optional prop, let's define a default value to be used in case we don't
  // have a roomStatus passed down
  static defaultProps = {
    // Empty component is perfect
    roomStatus: <Empty />,
  };

  render() {
    const {
      cwpButton,
      roomStatus,
    } = this.props;


    // This will inject cwpId as a prop to supplied component
    const renderCwpButton = clientId => React.cloneElement(cwpButton, {clientId});

    return (
      <Column>
        <Row
          justifyContent="center"
        >
          <Column>
            {renderCwpButton(23)}
            {renderCwpButton(22)}
            {renderCwpButton(21)}
            {renderCwpButton(20)}
          </Column>
          <Column>
            {renderCwpButton(24)}
            {renderCwpButton(25)}
            {renderCwpButton(26)}
            {renderCwpButton(27)}
          </Column>
        </Row>
        <Row justifyContent="center">
          <Column
            flexBasis="0"
            flexGrow={1}
            alignItems="flex-end"
          >
            <Row>
              {renderCwpButton(14)}
              {renderCwpButton(15)}
              {renderCwpButton(16)}
            </Row>
            <Row>
              {renderCwpButton(13)}
              {renderCwpButton(12)}
              {renderCwpButton(11)}
            </Row>
          </Column>
          <Row
            justifyContent="center"
            alignItems="center"
          >
            {roomStatus}
          </Row>
          <Column
            flexBasis="0"
            flexGrow={1}
          >
            <Row>
              {renderCwpButton(30)}
              {renderCwpButton(31)}
              {renderCwpButton(32)}
              {renderCwpButton(33)}
            </Row>
            <Row>
              {renderCwpButton(37)}
              {renderCwpButton(36)}
              {renderCwpButton(35)}
              {renderCwpButton(34)}
            </Row>
          </Column>
        </Row>
      </Column>
    );
  }
}

export default ControlRoomLayout;
