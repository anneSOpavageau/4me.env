// @flow
export default [
  // Cluster 1 : ZXNH
  [
    // ZX
    ['Z1','X1','Z2','X2','Z3','X3','Z4','X4'],
    // NH
    ['NH1', 'N2', 'H2','N3','H3','N4','H4'],
  ],
  // Cluster 2 : RLPT
  [
    // RL
    ['R1', 'L1', 'R2', 'L2','R3','L3','R4','L4'],
    // PT
    ['P12','P3','P4','T12','T3','T4'],    
  ],
 // Cluster 3 : FIR
  [
    // FIR
    ['TZ', 'TG', 'LM', 'BN'],   
  ],

];
