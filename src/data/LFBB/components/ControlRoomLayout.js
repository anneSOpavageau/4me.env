// @flow
import React, { Component } from 'react';

import Flexbox from 'flexbox-react';


// Helper components around flexbox-react
const Row = ({
  children,
  ...rest
} : {
  children?: React.Element<*>,
}): React.Element<*> =>
  React.createElement(Flexbox, {...rest, flexDirection: 'row'}, children);

const Column = ({
  children,
  ...rest
} : {
  children?: React.Element<*>,
}): React.Element<*> =>
  React.createElement(Flexbox, {...rest, flexDirection: 'column'}, children);


// This is an empty react component, just a function that returns null
const Empty = () => null;

// This is FlowType. We define an object type called Props that will be used in our component definition below
type Props = {
  cwpButton: React.Element<*>,
  // This prop is optional (see question mark)
  roomStatus?: React.Element<*>,
};

/**
 * This component controls the general layout of the control room
 * The idea is to allow parent to provide two sub components as props :
 * * A CwpButton, which will get a cwpId injected as prop
 * * An optional RoomStatus
 * The layout should be built using Flexbox
 * This allows the layout to be reused in different places where the container size changes
 * For instance, this layout could be used in a widget context or a modal context
 * This layout is used multiple contexts in `4me.frontend` :
 *   * Control room main page
 *   * Control room widget
 */
class ControlRoomLayout extends Component {
  // Here, we instruct flowtype that this component expects props matching the type Props
  props: Props;

  // Even though we use flowtype, we keep regular proptypes in place :
  // Users of this library might not have flowtype setup
  static propTypes = {
    cwpButton: React.PropTypes.element.isRequired,
    roomStatus: React.PropTypes.element,
  };

  // Since roomStatus is an optional prop, let's define a default value to be used in case we don't
  // have a roomStatus passed down
  static defaultProps = {
    // Empty component is perfect
    roomStatus: <Empty />,
  };

  render() {
    const {
      cwpButton,
      roomStatus,
    } = this.props;


    // This will inject cwpId as a prop to supplied component
    // To have roomstatus in the middle, a empty column is created
    // as the beginning of the row containing 3 columns : 1 empty,
    // 1 with room and 1 with position 17 and 18.
    const renderCwpButton = clientId => React.cloneElement(cwpButton, {clientId});

    return (
    <Column>
          <Row
          justifyContent="space-between"
                    >
              <Column justifyContent="flex-start">
                <Row justifyContent="flex-end">
                    {renderCwpButton(31)}
                    {renderCwpButton(30)}
                </Row>
                <Row justifyContent="flex-start">
                    {renderCwpButton(28)}
                    {renderCwpButton(29)}
                </Row>
              </Column>
              <Column justifyContent="flex-start">
                <Row>
                    {renderCwpButton(11)}
                    {renderCwpButton(12)}
                    {renderCwpButton(13)}
                    {renderCwpButton(14)}
                </Row>
                <Row justifyContent="flex-end">
                    {renderCwpButton(15)}
                    {renderCwpButton(16)}
                </Row>
              </Column>
            </Row>
            <Row justifyContent="space-between">
            <Column
              justifyContent="center"
              alignItems="flex-end"
            >

            </Column>
                  <Column
                    justifyContent="center"
                    alignItems="flex-end"
                  >
                 <Row justifyContent="flex-end">
                    {roomStatus}
                </Row>
                  </Column>
                <Column
                  justifyContent="center"
                  alignItems="center"
                >
                        <Row justifyContent="center">
                            {renderCwpButton(17)}
                        </Row>
                        <Row justifyContent="center">
                            {renderCwpButton(18)}
                        </Row>
                </Column>
              </Row>
              <Row
              justifyContent="space-between"
                        >
                          <Column justifyContent="flex-start">
                            <Row justifyContent="flex-end">
                                {renderCwpButton(27)}
                                {renderCwpButton(25)}
                            </Row>
                            <Row justifyContent="flex-start">
                                {renderCwpButton(26)}
                                {renderCwpButton(24)}
                            </Row>
                          </Column>
                          <Column justifyContent="flex-end">
                            <Row justifyContent="flex-end">
                                {renderCwpButton(20)}
                                {renderCwpButton(19)}
                            </Row>
                            <Row justifyContent="flex-start">
                                {renderCwpButton(23)}
                                {renderCwpButton(22)}
                                {renderCwpButton(21)}
                            </Row>
                          </Column>
                </Row>
     </Column>
    );
  }
}

export default ControlRoomLayout;
