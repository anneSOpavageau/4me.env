// @flow
// Mettre les bonnes fréquences selon les secteurs déclarés sur la position
const zone1 = [
  {
    id: 11,
    name: 'B31',
    type: 'cwp',
    suggestions: {
      filteredSectors: [],
      preferenceOrder: [
        "ZXNH1",
        "ZX12",
        "NH12",
        "ZX1",
        "NH1"
      ]
    },
    backupedRadios: [
      "133.680",
      "118.430"
    ]
  }, {
    id: 12,
    name: 'B32',
    type: 'cwp',
    suggestions: {
      filteredSectors: [
        "ZXNH",
        "ZXNH123",
        "ZXNH12",
        "ZXNH2",
        "ZX12",
        "NH12",
        "ZX2",
        "NH2"
      ],
      preferenceOrder: [
        "ZXNH",
        "ZXNH123",
        "ZXNH12",
        "ZXNH2",
        "ZX12",
        "NH12",
        "ZX2",
        "NH2"
      ]
    },
    backupedRadios: [
      "118.430",
      "122.415",
    ]
  }, {
    id: 13,
    name: 'B33',
    type: 'cwp',
    suggestions: {
      filteredSectors: [
        "ZXNH34",
        "ZXNH3",        
        "ZX3",
        "NH3"],
      preferenceOrder: []
    },
    backupedRadios: [
      "136.055"
    ]
  }, {
    id: 14,
    name: 'B34',
    type: 'cwp',
    suggestions: {
      filteredSectors: [
        "ZXNH34",
        "ZXNH4",        
        "ZX4",
        "NH4"
      ],
      preferenceOrder: []
    },
    backupedRadios: [
      "136.055"
    ]
  }, {
    id: 15,
    name: 'B21',
    type: 'cwp',
    backupedRadios: [],
    suggestions: {
      filteredSectors: [
         "FIR",
         "FIRN",
         ],
      preferenceOrder: []
    }
  }, 
  {
    id: 16,
    name: 'B22',
    type: 'cwp',
    backupedRadios: [],
    suggestions: {
      filteredSectors: [
          "FIRS"
           ],
      preferenceOrder: []
    }
  },
];

export default zone1;
