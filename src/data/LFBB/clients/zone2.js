// @flow
const zone2 = [
  {
    id: 18,
    name: 'A11',
    type: 'cwp',
    backupedRadios: [
      '132.180',
      '133.960',
    
    ],
    suggestions: {
      filteredSectors: ["RLPT"],
      preferenceOrder: ["PT", "RL"]
    },
  }, {
    id: 19,
    name: 'A22',
    type: 'cwp',
    backupedRadios: [
      '124.635'
    ],
    suggestions: {
      filteredSectors: ["T"],
      preferenceOrder: ["T4"]
    },
  }, {
    id: 20,
    name: 'A21',
    type: 'cwp',
    backupedRadios: [
      '123.630',
      '133.465'
    ],
    suggestions: {
      filteredSectors: ["T"],
      preferenceOrder: ["T123"]
    },
  }, {
    id: 21,
    name: 'A33',
    type: 'cwp',
    backupedRadios: [
      '132.990'
    ],
    suggestions: {
      filteredSectors: ["RL"],
      preferenceOrder: ["RL12"]
    },
  }, {
    id: 22,
    name: 'A32',
    type: 'cwp',
    backupedRadios: [
      '135.240'
    ],
    suggestions: {
      filteredSectors: ["RL"],
      preferenceOrder: ["RL1"]
    },
  }, {
    id: 23,
    name: 'A31',
    type: 'cwp',
    backupedRadios: [
      '135.240',
      '134.255'
    ],
    suggestions: {
      filteredSectors: ["RL"],
      preferenceOrder: ["RL1"]
    },
  }
];

export default zone2;
