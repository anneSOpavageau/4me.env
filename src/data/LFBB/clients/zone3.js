// @flow
const zone3 = [
  {
    id: 25,
    name: 'A41',
    type: 'cwp',
    suggestions: {
      filteredSectors: [
        "RL"
      ],
      preferenceOrder: [
      ]
    },
    backupedRadios: [
      "135.240"
    ]
  }, {
    id: 24,
    name: 'A42',
    type: 'cwp',
    suggestions: {
      filteredSectors: [
        "RL"
      ],
      preferenceOrder: [        
      ]
    },
    backupedRadios: [
      "134.255"
    ]
  }, {
    id: 27,
    name: 'A51',
    type: 'cwp',
    suggestions: {
      filteredSectors: [
        "RL"
      ],
      preferenceOrder: [
      ]
    },
    backupedRadios: [
      "127.435"
    ]
  }, {
    id: 26,
    name: 'A52',
    type: 'cwp',
    suggestions: {
      filteredSectors: [
        "RL"
      ],
      preferenceOrder: [        
      ]
    },
    backupedRadios: [
      "134.255"
    ]
  },
];

export default zone3;
