// @flow
const zone4 = [
  {
    id: 30,
    name: 'B41',
    type: 'cwp',
    suggestions: {
      filteredSectors: [
        "ZXNH"
      ],
      preferenceOrder: [
      ]
    },
    backupedRadios: [
      "122.415",
      "126.130"
    ]
  }, {
    id: 31,
    name: 'B42',
    type: 'cwp',
    suggestions: {
      filteredSectors: [
        "ZXNH"
      ],
      preferenceOrder: [        
      ]
    },
    backupedRadios: [
      "136.055",
      "120.935"
    ]
  }, {
    id: 29,
    name: 'B51',
    type: 'cwp',
    suggestions: {
      filteredSectors: [
        "ZXNH"
      ],
      preferenceOrder: [
      ]
    },
    backupedRadios: [
     "136.055",
      "120.935"
    ]
  }, {
    id: 28,
    name: 'B52',
    type: 'cwp',
    suggestions: {
      filteredSectors: [
        "ZXNH"
      ],
      preferenceOrder: [        
      ]
    },
    backupedRadios: [
      "136.055",
      "120.935"
    ]
  },
];

export default zone4;
