// @flow
const frequencies = [
  // FIR
  {
    name: '127.675',
    defaultSector: 'LM',
  },
  {
    name: '128.025',
    defaultSector: 'TG',
  },
  {
    name: '129.100',
    defaultSector: 'TZ',
  },
 {
    name: '125.105',
    defaultSector: 'BN',
  },
  // RL
  {
    name: '132.430',
    defaultSector: 'R1',
  },
  {
    name: '133.580',
    defaultSector: 'R2',
  },
  {
    name: '133.010',
    defaultSector: 'R3',
  },
  {
    name: '122.630',
    defaultSector: 'R4',
  },
{
    name: '132.990',
    defaultSector: 'L1',
  },
  {
    name: '135.240',
    defaultSector: 'L2',
  },
  {
    name: '134.255',
    defaultSector: 'L3',
  },
  {
    name: '127.435',
    defaultSector: 'L4',
  },

  // P: fréquences à vérifier
  {
    name: '132.180',
    defaultSector: 'P12',
  },
  {
    name: '133.960',
    defaultSector: 'P3',
  },
  {
    name: '122.535',
    defaultSector: 'P4',
  },

  // T
  {
    name: '124.635',
    defaultSector: 'T4',
  },
  {
    name: '133.465',
    defaultSector: 'T3',
  },
{
    name: '123.630',
    defaultSector: 'T12',
  },
  // Z
  {
    name: '128.765',
    defaultSector: 'Z4',
  },
{
    name: '134.765',
    defaultSector: 'Z3',
  },
{
    name: '121.340',
    defaultSector: 'Z2',
  },
{
    name: '133.680',
    defaultSector: 'Z1',
  },
  // X
  {
    name: '135.115',
    defaultSector: 'X4',
  },
  {
    name: '122.415',
    defaultSector: 'X3',
  },
  {
    name: '126.130',
    defaultSector: 'X2',
  },
  {
    name: '118.430',
    defaultSector: 'X1',
  },
  // N
  {
    name: '136.055',
    defaultSector: 'N4',
  },
  {
    name: '120.935',
    defaultSector: 'N3',
  },
  {
    name: '124.825',
    defaultSector: 'N2',
  },
  {
    name: '132.415',
    defaultSector: 'NH1',
  },
// H
  {
    name: '134.610',
    defaultSector: 'H4',
  },
  {
    name: '124.080',
    defaultSector: 'H3',
  },
  {
    name: '132.910',
    defaultSector: 'H2',
  },
  // Backup frequencies
// j'en suis là :comment traiter les uhf et les supplétives
  {
    name: '134.725',
    defaultSector: null,
  },
  {
    name: '132.630',
    defaultSector: null,
  },
  {
    name: '135.105',
    defaultSector: null,
  },
];

export default frequencies;
