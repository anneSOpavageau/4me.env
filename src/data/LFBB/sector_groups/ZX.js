// @flow
const ZoulouXray = [
  {
    "name":"Z1",
    "elementarySectors":["Z1"],
    "canAccept":[
      "X1",      
    ],
    "canGive":[]
  },
  {
    "name":"X1",
    "elementarySectors":["X1"],
    "canAccept":[
      "Z1",      
    ],
    "canGive":[]
  },
  {
    "name":"Z2",
    "elementarySectors":["Z2"],
    "canAccept":[
      "X2"
    ],
    "canGive":[]
  },
  {
    "name":"X2",
    "elementarySectors":["X2"],
    "canAccept":[
      "Z2",
    ],
    "canGive":[]
  },
  {
    "name":"X3",
    "elementarySectors":[
      "X3"
    ],
    "canAccept":[
      "Z3"
    ],
    "canGive":[     
    ]
  },
  {
    "name":"Z3",
    "elementarySectors":[
      "Z3"
    ],
    "canAccept":[
      "X3"
    ],
    "canGive":[     
    ]
  },
  {
    "name":"Z4",
    "elementarySectors":[
      "Z4"
    ],
    "canAccept":[
      "X4"
    ],
    "canGive":[      
    ]
  },
  {
    "name":"X4",
    "elementarySectors":[
      "X4"
    ],
    "canAccept":[
      "Z4"
    ],
    "canGive":[
    ]
  },
  {
    "name":"ZX1",
    "elementarySectors":[
      "X1",
      "Z1"
    ],
    "canAccept":[
      "NH1",
      "ZX2"
    ],
    "canGive":[
      "Z1",
      "X1",
    ]
  },
  {
    "name":"ZX2",
    "elementarySectors":[
      "Z2",
      "X2"
    ],
    "canAccept":[
      "ZX1",
      "NH2"
    ],
    "canGive":[
      "Z2",
      "X2"
    ]
  },
{
    "name":"ZX12",
    "elementarySectors":[
      "Z1",
      "X1",
      "Z2",
      "X2",
    ],
    "canAccept":[
      "NH12"
    ],
    "canGive":[
      "ZX1",
      "ZX2"
    ]
  },
{
    "name":"ZX3",
    "elementarySectors":[
      "Z3",
      "X3"
    ],
    "canAccept":[
      "ZX4",
      "NH3"
    ],
    "canGive":[
      "Z3",
      "X3"
    ]
  },
{
    "name":"ZX4",
    "elementarySectors":[
      "Z4",
      "X4"
    ],
    "canAccept":[
      "ZX3",
      "NH4"
    ],
    "canGive":[
      "Z4",
      "X4"
    ]
  }
];

export default ZoulouXray;
