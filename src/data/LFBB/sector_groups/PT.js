// @flow
const PAPATANGO = [
  {
    "name":"PT",
    "elementarySectors":[
    "P12",
    "P3",
    "P4",
    "T12",
    "T3",
    "T4",
     ],
    "canAccept":[
      "RL"
    ],
    "canGive":[
    "P",
    "T"
    ]
  }
];

export default PAPATANGO;
