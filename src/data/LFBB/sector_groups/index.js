// @flow
import FIR from './FIR';
import NovHotel from './NH';
import PAPA from './P';
import PAPATANGO from './PT';
import RomeoLima from './RL';
import RomeoLimaPapaTango from './RLPT';
import TANGO from './T';
import UIR from './UIR';
import ZoulouXray from './ZX';
import ZoulouXrayNovHotel from './ZXNH';

export default [
  ...FIR,
  ...NovHotel,
  ...PAPA,
  ...PAPATANGO,
  ...RomeoLima,
  ...RomeoLimaPapaTango,
  ...TANGO,
  ...UIR,
  ...ZoulouXray,
  ...ZoulouXrayNovHotel
];
