// @flow
const RomeoLimaPapaTango = [
  {
    "name":"RLPT",
    "elementarySectors":[
      "R1",
      "R2",
      "R3",
      "R4",
      "L1",
      "L2",
      "L3",
      "L4",
      "P12",
      "P3",
      "P4",
      "T12",
      "T3",
      "T4"
     ],
    "canAccept":[
      "ZXNH"      
    ],
    "canGive":[
      "RL",
      "PT"
     ]
  }
];

export default RomeoLimaPapaTango;
