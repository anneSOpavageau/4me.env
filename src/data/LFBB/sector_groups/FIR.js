// @flow
const FIR = [
  {
    "name":"LM",
    "elementarySectors":["LM"],
    "canAccept":[
      "BN"
    ],
    "canGive":[]
  },
  {
    "name":"BN",
    "elementarySectors":["BN"],
    "canAccept":[
      "LM"
    ],
    "canGive":[]
  },
  {
    "name":"TZ",
    "elementarySectors":[
      "TZ"
    ],
    "canAccept":[
      "TG"
    ],
    "canGive":[
    ]
  },
{
    "name":"TG",
    "elementarySectors":[
      "TG"
    ],
    "canAccept":[
      "TZ"
    ],
    "canGive":[
    ]
  },
 {
    "name":"FIRN",
    "elementarySectors":[
      "LM",
      "BN"
    ],
    "canAccept":[
      "FIRS"
    ],
    "canGive":[
      "LM",
      "BN"
    ]
  },
  {
    "name":"FIRS",
    "elementarySectors":[
      "TZ",
      "TG"
    ],
    "canAccept":[
      "FIRN"
    ],
    "canGive":[
      "TZ",
      "TG"
    ]
  },
{
    "name":"FIR",
    "elementarySectors":[
      "TZ",
      "TG",
      "LM",
      "BN"
    ],
    "canAccept":[
    ],
    "canGive":[     
      "FIRN",
      "FIRS",
    ]
  }


];

export default FIR;
