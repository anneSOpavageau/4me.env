// @flow
const UIR = [
  {
    "name":"UIR",
    "elementarySectors":[
     "R1",
      "R2",
      "R3",
      "R4",
      "L1",
      "L2",
      "L3",
      "L4",
      "P12",
      "P3",
      "P4",
      "T12",
      "T3",
      "T4",
     "Z1",
     "X1",
     "NH1",
     "Z2",
     "X2",
     "N2",
     "H2",
     "Z3",
     "X3",
     "N3",
     "H3",
     "Z4",
     "X4",
     "N4",
     "H4",
    ],
    "canAccept":[
    ],
    "canGive":[
    "RLPT",
    "ZXNH"
     ]  
  }
];

export default UIR;
