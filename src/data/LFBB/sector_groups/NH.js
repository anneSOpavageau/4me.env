// @flow
const NovHotel = [
  {
    "name":"NH1",
    "elementarySectors":["NH1"],
    "canAccept":[
      "NH2"
    ],
    "canGive":[]
  },
  {
    "name":"N2",
    "elementarySectors":["N2"],
    "canAccept":[
      "H2"
    ],
    "canGive":[]
  },
  {
    "name":"H2",
    "elementarySectors":["H2"],
    "canAccept":[
      "N2"
    ],
    "canGive":[]
  },
{
    "name":"NH12",
    "elementarySectors":[     
      "H2",
      "NH1",
      "N2"
],
    "canAccept":[
      "ZX12"
    ],
    "canGive":[
      "ZX1",
      "ZX2"
    ]
  },
  {
    "name":"N3",
    "elementarySectors":[
      "N3"
    ],
    "canAccept":[
      "H3"
    ],
    "canGive":[     
    ]
  },
  {
    "name":"H3",
    "elementarySectors":[
      "H3"
    ],
    "canAccept":[
      "N3"
    ],
    "canGive":[     
    ]
  },
  {
    "name":"H4",
    "elementarySectors":[
      "H4"
    ],
    "canAccept":[
      "N4"
    ],
    "canGive":[      
    ]
  },
  {
    "name":"N4",
    "elementarySectors":[
      "N4"
    ],
    "canAccept":[
      "H4"
    ],
    "canGive":[
    ]
  },  
  {
    "name":"NH2",
    "elementarySectors":[
      "N2",
      "H2"
    ],
    "canAccept":[
      "ZX2",
      "NH1"
    ],
    "canGive":[
      "N2",
      "H2"
    ]
  },
{
    "name":"NH3",
    "elementarySectors":[
      "N3",
      "H3"
    ],
    "canAccept":[
      "ZX3",
      "NH4"
    ],
    "canGive":[
      "N3",
      "H3"
    ]
  },
{
    "name":"NH4",
    "elementarySectors":[
      "N4",
      "H4"
    ],
    "canAccept":[
      "ZX4",
      "NH3"
    ],
    "canGive":[
      "N4",
      "H4"
    ]
  }
];

export default NovHotel;
