// @flow
import clusters from './clusters';
import sectorGroups from './sector_groups';
import clients from './clients';
import frequencies from './frequencies';
import xman from './xman';

import type { EnvironmentData } from '../../types';

const LFBB: EnvironmentData = {
  clusters,
  sectorGroups,
  clients,
  frequencies,
  xman,
};

export default () => {
  // Now, we check wether is react is requirable
  // If so, add components to the API
  try {
    // Check wether react is installed
    require.resolve('react');
    // If so, require components
    const components = require('./components').default;

    return {
      ...LFBB,
      components,
    };
  } catch(err) {
    return LFBB;
  }
};
