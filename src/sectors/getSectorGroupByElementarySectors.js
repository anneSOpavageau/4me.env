// @flow
import R from 'ramda';
import invariant from 'invariant';

import getElementarySectors from './getElementarySectors';

import type {
  SectorGroup,
  EnvironmentData,
  ElementarySector,
} from '../types';

export default function getSectorGroupByElementarySectors(
  data: EnvironmentData,
  elementarySectors: Array<ElementarySector>,
): ?SectorGroup {
  invariant(
    // Check presence of argument
    elementarySectors &&
    // Check argument is an array
    Array.isArray(elementarySectors) &&
    // Check argument contains at least one element
    elementarySectors.length > 0 &&
    // Check each item in argument is an actual elementary sector
    R.isEmpty(R.without(getElementarySectors(data), elementarySectors)),
    'Invalid argument',
  );

  const withElementarySectorsMatching = (elementarySectors: ElementarySector[]) => R.pipe(
    R.prop('elementarySectors'),
    R.symmetricDifference(elementarySectors),
    R.isEmpty,
  );

  type FindByES = ElementarySector[] => SectorGroup[] => ?SectorGroup;
  const findByEs: FindByES = elementarySectors => R.find(withElementarySectorsMatching(elementarySectors));

  return findByEs(elementarySectors)(data.sectorGroups);
};
