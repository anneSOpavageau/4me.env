// @flow
import R from 'ramda';

import type {
  EnvironmentData,
  ElementarySector,
} from '../types';

export default function getElementarySectors(
  data: EnvironmentData,
): Array<ElementarySector> {
  return R.flatten(data.clusters);
}
