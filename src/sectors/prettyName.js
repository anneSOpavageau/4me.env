// @flow
import invariant from 'invariant';
import R from 'ramda';


import type {
  ElementarySector,
  SectorGroup,
  EnvironmentData,
} from '../types';

import getSectorGroupByName from './getSectorGroupByName';
import getSectorGroups from './getSectorGroups';
import getSectorGroupByElementarySectors from './getSectorGroupByElementarySectors';

// :: ([ElementarySector], [SectorGroup]) -> Number
//
type SectorsInCommon = ((elementarySectors: ElementarySector[], sectorGroup: SectorGroup) => number)
export const getSectorsInCommon: SectorsInCommon =
  (elementarySectors: ElementarySector[], sectorGroup: SectorGroup): number => {
    // First check if any elementarySector in the sectorGroup is NOT in supplied elementarySectors
    const isIncluded: (SectorGroup => boolean) = R.pipe(
      // Get elementary sectors from our sector group
      R.prop('elementarySectors'),
      // Remove supplied elementary sectors
      R.without(elementarySectors),
      // Count what's remaining
      R.length,
      // Returns true if nothing remains
      R.equals(0),
    );

    if(!isIncluded(sectorGroup)) {
      return -1;
    }

    // At this point, we know our current sectorGroup is included, let's add a score to it
    const getScore: (SectorGroup => number) = R.pipe(
      // Get elementary sectors from our sector group
      R.prop('elementarySectors'),
      // Remove them from supplied elementary sectors
      R.without(R.__, elementarySectors),
      // Count what's left (i.e supplied elementary sectors not in the current sector group)
      R.length,
      // Subtract what's left from elementarySectors length
      R.subtract(R.length(elementarySectors)),
    );

    return getScore(sectorGroup);
  };


export default function prettyName(
  data: EnvironmentData,
  elementarySectors: Array<ElementarySector>,
  separator: string = ',',
): string {
  if(!elementarySectors || R.isEmpty(elementarySectors)) {
    return '';
  }

  const sectorGroup = getSectorGroupByElementarySectors(data, elementarySectors);
  if(sectorGroup) {
    return sectorGroup.name;
  }

  // Ok at this point, we have no matching sector group
  // Let's try to find the largest one contained in our supplied argument

  const sectorGroups = getSectorGroups(data);

  type FindFirstCandidate = Array<ElementarySector> => Array<SectorGroup> => ?SectorGroup
  const findFirstCandidate: FindFirstCandidate = elementarySectors => R.pipe(
    R.sortBy(sectorGroup => getSectorsInCommon(elementarySectors, sectorGroup)),
    // Keep proposals with at least 2 sectors in common only
    R.reject(sectorGroup => getSectorsInCommon(elementarySectors, sectorGroup) < 2),
    R.reverse,
    R.head,
  );

  const firstCandidate: ?SectorGroup = findFirstCandidate(elementarySectors)(sectorGroups);
  if(!firstCandidate) {
    return elementarySectors.join(separator);
  }

  const remainingSectors: Array<ElementarySector> = R.without(firstCandidate.elementarySectors, elementarySectors);
  let rest = remainingSectors.join(separator);

  // We have more, go deeper !
  // http://i.imgur.com/6KwZgMg.jpg
  if(R.length(remainingSectors) >= 2) {
    rest = prettyName(data, remainingSectors, separator);
  }

  return [
    firstCandidate.name,
    rest,
  ].join(separator);

}
