// @flow

import type {
  SectorGroup,
  EnvironmentData,
} from '../types';

export default function getSectorGroups(data: EnvironmentData): Array<SectorGroup> {
  return data.sectorGroups;
}
