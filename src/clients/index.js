// @flow
import R from 'ramda';
import invariant from 'invariant';

export const validClientTypes = [
  'flow-manager',
  'supervisor',
  'tech-supervisor',
  'cwp',
];

import type {
  EnvironmentData,
  Client,
  ClientId,
  Suggestions,
  Map,
} from '../types';

import getSectorSuggestions from './getSectorSuggestions';
import getClients from './getClients';
import getClientById from './getClientById';

export type ClientAPI = {|
  getClients: void => Array<Client>,
  getClientById: ClientId => ?Client,
  getSectorSuggestions: (Map, ClientId) => Suggestions,
|};


const getClientsAPI = (data: EnvironmentData): ClientAPI => {
  invariant(
    data,
    'Invalid data provided',
  );

  invariant(
    data.clients,
    'Invalid data provided',
  );

  // Let's define our sector API here
  return {
    getClients: getClients.bind(null, data),
    getClientById: getClientById.bind(null, data),
    getSectorSuggestions: getSectorSuggestions.bind(null, data),
  };
};

export default getClientsAPI;
