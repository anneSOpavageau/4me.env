// @flow
import invariant from 'invariant';
import R from 'ramda';

import type {
  EnvironmentData,
  ClientId,
  Client,
} from '../types';

export default function getClientById(data: EnvironmentData, clientId: ClientId): ?Client {
  return R.find(R.propEq('id', clientId), data.clients) || null;
}
