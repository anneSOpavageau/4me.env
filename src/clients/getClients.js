// @flow

import type {
  EnvironmentData,
  Client,
} from '../types';

export default function getClients(data: EnvironmentData): Array<Client> {
  return data.clients;
}
